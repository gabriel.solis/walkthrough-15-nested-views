/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"Tutoriales/Walkthrough-15-Nested-Views/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});